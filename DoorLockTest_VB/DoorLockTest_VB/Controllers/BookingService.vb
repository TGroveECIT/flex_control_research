﻿Imports System.IO
Imports System.Net.Sockets
Imports System.Text
Imports System.Xml.Serialization
Imports DoorLockTest_VB.Models

Public Class BookingService
    Private Shared ReadOnly host = "booking.flex-control.com"
    'Private host = "193.88.185.66"
    Private Shared ReadOnly port = 4001

    'WTF! 😊❤
    Private Shared ReadOnly smiley = 2
    Private Shared ReadOnly heart = 3


    Public Shared Function SendQuery(ByVal query As String) As List(Of Booking)


        Dim index As Integer = 0
        Dim counter As Integer = 0
        Do
            index = query.IndexOf($"confirmation={Chr(34)}", index + 1)
            If index > 0 Then
                counter += 1
            End If
        Loop While index > 0


        Dim client = New TcpClient(host, port)
        Dim stream = client.GetStream

        Dim data As [Byte]() = Encoding.UTF8.GetBytes(Chr(smiley) & query & Chr(heart))
        stream.Write(data, 0, data.Length)
        Dim responses = New List(Of String)
        Dim response As String

        For index = 1 To counter
            response = ""
            While True
                Dim Datas = New Byte(0) {2}
                If stream.DataAvailable Then
                    Dim bytes As Int32 = stream.Read(Datas, 0, Datas.Length)
                End If
                Dim value = Convert.ToInt32(Datas.FirstOrDefault)

                If value = 3 Then
                    Exit While
                ElseIf Not value = 2 And Not value = 3 Then
                    response += Chr(value)
                End If
            End While
            responses.Add(response)
        Next

        Dim list = New List(Of Booking)
        For Each res In responses
            list.Add(Deserialize(res))
        Next

        stream.Close()
        client.Close()
        Return list

    End Function

    Private Shared Function Deserialize(ByVal val As String)
        Dim serializer = New XmlSerializer(GetType(Booking))
        Dim booking As Booking
        Using reader As New StringReader(val)
            booking = serializer.Deserialize(reader)
        End Using

        Return booking
    End Function


End Class
