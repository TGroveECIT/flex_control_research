﻿Imports System.IO
Imports System.IO.Compression

Public Class GZipHelper
    Public Shared Function Compress(ByVal data As Byte()) As Byte()
        Using compressedStream = New MemoryStream()

            Using zipStream = New GZipStream(compressedStream, CompressionMode.Compress)
                zipStream.Write(data, 0, data.Length)
                zipStream.Close()
                Return compressedStream.ToArray()
            End Using
        End Using
    End Function

    Public Shared Function Decompress(ByVal data As Byte()) As Byte()
        Using compressedStream = New MemoryStream(data)

            Using zipStream = New GZipStream(compressedStream, CompressionMode.Decompress)

                Using resultStream = New MemoryStream()
                    zipStream.CopyTo(resultStream)
                    Return resultStream.ToArray()
                End Using
            End Using
        End Using
    End Function

    Public Shared Function StringToByteArray(ByVal hex As String) As Byte()
        Dim NumberChars As Integer = hex.Length
        Dim bytes As Byte() = New Byte(NumberChars / 2 - 1) {}

        For i As Integer = 0 To NumberChars - 1 Step 2
            bytes(i / 2) = Convert.ToByte(hex.Substring(i, 2), 16)
        Next

        Return bytes
    End Function
End Class
