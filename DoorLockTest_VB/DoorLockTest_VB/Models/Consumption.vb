﻿Imports System
Imports System.Xml.Serialization
Imports System.Collections.Generic

Namespace Models
    <XmlRoot(ElementName:="consumption")>
    Public Class Consumption
        <XmlAttribute(AttributeName:="confirmation")>
        Public Property Confirmation As String
        <XmlAttribute(AttributeName:="units")>
        Public Property Units As String
        <XmlAttribute(AttributeName:="error")>
        Public Property [Error] As String
        <XmlAttribute(AttributeName:="type")>
        Public Property Type As String
        <XmlText>
        Public Property Text As String
    End Class
End Namespace