﻿Imports System.Xml.Serialization

Public Class Alive

    <XmlAttribute(AttributeName:="confirmation")>
    Public Property Confirmation As String
    <XmlAttribute(AttributeName:="error")>
    Public Property [Error] As String
    <XmlAttribute(AttributeName:="connectdatetime")>
    Public Property ConnectDateTime As String
End Class
