﻿Imports System
Imports System.Xml.Serialization
Imports System.Collections.Generic

Namespace Models
    <XmlRoot(ElementName:="doorlock")>
    Public Class Doorlock
        <XmlAttribute(AttributeName:="confirmation")>
        Public Property Confirmation As String
        <XmlText>
        Public Property Text As String
    End Class
End Namespace