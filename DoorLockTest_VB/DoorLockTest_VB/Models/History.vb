﻿
Imports System
Imports System.Xml.Serialization
Imports System.Collections.Generic
Imports System.Text

Namespace Models
    <XmlRoot(ElementName:="history")>
    Public Class History
        <XmlAttribute(AttributeName:="data")>
        Public Property Data As String
        <XmlAttribute(AttributeName:="startdatetime")>
        Public Property Startdatetime As String
        <XmlAttribute(AttributeName:="enddatetime")>
        Public Property Enddatetime As String
        <XmlAttribute(AttributeName:="compression")>
        Public Property Compression As String
        <XmlAttribute(AttributeName:="confirmation")>
        Public Property Confirmation As String
        <XmlAttribute(AttributeName:="precision")>
        Public Property Precision As String
        <XmlAttribute(AttributeName:="truncated")>
        Public Property Truncated As String
        <XmlAttribute(AttributeName:="error")>
        Public Property [Error] As String
        <XmlText>
        Public Property Text As String

        Public Shared Function CalculateConsumption(gzipString As String) As Integer
            Dim hexAsByteArr As Byte() = GZipHelper.StringToByteArray(gzipString)
            Dim decompressedByteArr As Byte() = GZipHelper.Decompress(hexAsByteArr)
            Dim decompressedStr As String = Encoding.ASCII.GetString(decompressedByteArr)
            Dim decompressedSub = decompressedStr.Split(vbLf)

            Return CalculateConsumption(decompressedSub.ToList)

            'Dim first = decompressedSub.First.Split(":")
            'Dim last = decompressedSub.ElementAt(decompressedSub.Length - 2).Split(":")
            'Dim last = decompressedSub.Last.Split(":")

            'Return Integer.Parse(last.Last) - Integer.Parse(first.Last)

        End Function

        Private Shared Function CalculateConsumption(list As List(Of String))
            'remove last element as it is always left empty
            list.RemoveAt(list.Count - 1)
            Dim syncs As New List(Of List(Of String))

            Dim syncCounter As Integer = 0

            'estimate how many lists needs to be traversed
            For Each consumptions In list
                If consumptions.Contains(";38") Then
                    syncCounter += 1
                End If
            Next

            'create lists to traverse
            If syncCounter > 0 Then
                For index = 0 To syncCounter
                    syncs.Add(New List(Of String))
                Next

                Dim elementIndex As Integer = 0
                Dim newTraversal As Boolean = True
                For counter = 0 To syncCounter
                    For index = elementIndex To list.Count - 1
                        If list.ElementAt(index).Contains(";38") Then
                            If newTraversal = True Then
                                syncs.ElementAt(counter).Add(list.ElementAt(index))
                                newTraversal = False
                                elementIndex += 1
                            Else
                                Exit For
                            End If
                        Else
                            syncs.ElementAt(counter).Add(list.ElementAt(index))
                            newTraversal = False
                            elementIndex += 1
                        End If
                    Next
                    newTraversal = True
                Next
            Else
                syncs.Add(list)
            End If


            Return CalculateConsumption(syncs)
        End Function

        Private Shared Function CalculateConsumption(syncLists As List(Of List(Of String))) As Integer
            Dim consumption As Integer = 0
            For Each item In syncLists
                'remove all occurrences of ";38" and ";0" before splitting and parsing
                Dim first = item.First.Replace(";38", "").Replace(";0", "").Split(":")
                Dim last = item.Last.Replace(";38", "").Replace(";0", "").Split(":")

                consumption += Integer.Parse(last.Last) - Integer.Parse(first.Last)
            Next
            Return consumption
        End Function

    End Class
End Namespace