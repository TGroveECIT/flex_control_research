﻿Imports System
Imports System.Xml.Serialization
Imports System.Collections.Generic

Namespace Models
    <XmlRoot(ElementName:="booking")>
    Public Class Booking
        <XmlElement(ElementName:="house")>
        Public Property House As House
        <XmlAttribute(AttributeName:="username")>
        Public Property Username As String
        <XmlAttribute(AttributeName:="password")>
        Public Property Password As String
    End Class
End Namespace