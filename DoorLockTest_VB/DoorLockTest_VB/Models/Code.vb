﻿Imports System.Xml.Serialization

Namespace Models
    <XmlRoot(ElementName:="code")>
    Public Class Code
        <XmlAttribute(AttributeName:="confirmation")>
        Public Property Confirmation As String
        <XmlAttribute(AttributeName:="error")>
        Public Property [Error] As String
        <XmlAttribute(AttributeName:="startdatetime")>
        Public Property StartDateTime As DateTime
        <XmlAttribute(AttributeName:="enddatetime")>
        Public Property EndDateTime As DateTime
        <XmlAttribute(AttributeName:="method")>
        Public Property Method As String
        <XmlAttribute(AttributeName:="radix")>
        Public Property Radix As String
        <XmlAttribute(AttributeName:="datetime")>
        Public Property StallDate As DateTime
        <XmlText>
        Public Property Text As String
    End Class
End Namespace