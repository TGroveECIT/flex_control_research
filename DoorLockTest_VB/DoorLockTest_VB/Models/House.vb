﻿Imports System
Imports System.Xml.Serialization
Imports System.Collections.Generic

Namespace Models
    <XmlRoot(ElementName:="house")>
    Public Class House
        <XmlElement(ElementName:="consumption")>
        Public Property Consumption As Consumption
        <XmlElement(ElementName:="doorlock")>
        Public Property Doorlock As Doorlock
        <XmlElement(ElementName:="history")>
        Public Property History As History
        <XmlElement(ElementName:="alive")>
        Public Property Alive As Alive
        <XmlElement(ElementName:="code")>
        Public Property Code As Code
        <XmlAttribute(AttributeName:="id")>
        Public Property Id As String
    End Class
End Namespace