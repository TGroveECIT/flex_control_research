﻿Imports System.Text

Public Class FlexControl_Door
    ''' <summary>
    ''' Query for unlocking a door
    ''' </summary>
    ''' <returns>Query in <see cref="String"/>-format</returns>
    Public Shared Function Unlock()
        Dim strBuilder = New StringBuilder
        strBuilder.Append($"<doorlock confirmation=""001"">0</doorlock>")
        Return strBuilder.ToString
    End Function

    ''' <summary>
    ''' Query for locking a door
    ''' </summary>
    ''' <returns>Query in <see cref="String"/>-format</returns>
    Public Shared Function Lock()
        Dim strBuilder = New StringBuilder
        strBuilder.Append("<doorlock confirmation=""001"">1</doorlock>"" />")
        Return strBuilder.ToString
    End Function

End Class
