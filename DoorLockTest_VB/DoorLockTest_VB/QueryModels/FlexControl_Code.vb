﻿Imports System.Text
Public Class FlexControl_Code
    Private Shared ReadOnly datetimeFormat As String = "yyMMddHHmmss"

    ''' <summary>
    ''' Creates a query command for sending a housecode to FlexControl
    ''' </summary>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <param name="method">values must be either "reset" or "add", defaults to "add"</param>
    ''' <param name="radix"></param>
    ''' <param name="stallDate"></param>
    ''' <returns></returns>
    Public Shared Function Code(startDate As DateTime, endDate As DateTime, newCode As String, Optional method As String = "add", Optional radix As String = Nothing, Optional stallDate As DateTime = Nothing) As String
        Dim stringBuilder As New StringBuilder
        stringBuilder.Append($"<code startdatetime=""{startDate.ToString(datetimeFormat)}"" expiredatetime=""{endDate.ToString(datetimeFormat)}"" method=""{method}"" ")

        If radix IsNot Nothing Then
            stringBuilder.Append($"radix=""{radix}"" ")
        End If

        If Not stallDate = Nothing Then
            stringBuilder.Append($"datetime=""{stallDate.ToString(datetimeFormat)}"" ")
        End If

        stringBuilder.Append($"confirmation=""006"">{newCode}</code>")

        Return stringBuilder.ToString
    End Function


End Class
