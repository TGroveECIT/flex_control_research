﻿Imports System.Text

Public Class FlexControl_Base

    ''' <summary>
    ''' Parent query for the FlexControl API, a command should be passed to this method.
    ''' </summary>
    ''' <param name="username"></param>
    ''' <param name="password"></param>
    ''' <param name="houseId"></param>
    ''' <param name="command"></param>
    ''' <returns>XMLQuery in <see cref="String"/>-format</returns>
    Public Shared Function XmlBaseQuery(ByVal username As String, ByVal password As String, ByVal houseId As String, ByVal command As String)
        Dim strBuilder = New StringBuilder
        strBuilder.Append("<?xml version=""1.0"" encoding=""UTF-8""?>")
        strBuilder.Append($"<booking username=""{username}"" password=""{password}"">")
        strBuilder.Append($"<house id=""{houseId}"">")
        strBuilder.Append(command)
        strBuilder.Append("</house>")
        strBuilder.Append("</booking>")
        Return strBuilder.ToString
    End Function

    ''' <summary>
    ''' Parent query for the FlexControl API, a <see cref="List(Of T)"/> of commands should be passed to this method.
    ''' This method should probably be ignored if the final response from FlexControl is important!
    ''' </summary>
    ''' <param name="username"></param>
    ''' <param name="password"></param>
    ''' <param name="houseId"></param>
    ''' <param name="commands"><see cref="List(Of T)"/>List of commands to be sent</param>
    ''' <returns>XMLQuery in <see cref="String"/>-format</returns>
    Public Shared Function XmlBaseQuery(ByVal username As String, ByVal password As String, ByVal houseId As String, ByVal commands As List(Of String))

        Dim strBuilder = New StringBuilder
        strBuilder.Append("<?xml version=""1.0"" encoding=""UTF-8""?>")
        strBuilder.Append($"<booking username=""{username}"" password=""{password}"">")
        strBuilder.Append($"<house id=""{houseId}"">")
        For Each command As String In commands
            strBuilder.Append(command)
        Next
        strBuilder.Append(Command)
        strBuilder.Append("</house>")
        strBuilder.Append("</booking>")
        Return strBuilder.ToString

    End Function


End Class
