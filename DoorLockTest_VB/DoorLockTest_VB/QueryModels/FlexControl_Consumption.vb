﻿Imports System.Text

Public Class FlexControl_Consumption



    ''' <summary>
    ''' Query for getting electricity consumption
    ''' </summary>
    ''' <returns>Query in <see cref="String"/>-format</returns>
    Public Shared Function ElectricityConsumption()
        Dim strBuilder = New StringBuilder
        strBuilder.Append("<consumption type=""electricity"" units=""kWh"" confirmation=""002"" />")
        Return strBuilder.ToString
    End Function

    ''' <summary>
    ''' Query for getting water consumption
    ''' </summary>
    ''' <returns>Query in <see cref="String"/>-format</returns>
    Public Shared Function WaterConsumption()
        Dim strBuilder = New StringBuilder
        strBuilder.Append("<consumption type=""water"" units=""L"" confirmation=""003"" />")
        Return strBuilder.ToString
    End Function

    ''' <summary>
    ''' Query for getting electricity consumption for a given period
    ''' </summary>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns></returns>
    Public Shared Function ElectricityConsumption(ByVal startDate As DateTime, ByVal endDate As DateTime)
        Return FlexControl_History.History(startDate, endDate, “consumption;electricity;kWh”, "2")
    End Function

    ''' <summary>
    ''' Query for getting water consumption for a given period
    ''' </summary>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <returns></returns>
    Public Shared Function WaterConsumption(ByVal startDate As DateTime, ByVal endDate As DateTime)
        Return FlexControl_History.History(startDate, endDate, “consumption;water;L”, "2")
    End Function

End Class
