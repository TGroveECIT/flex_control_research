﻿Class FlexControl_History
    Private Shared ReadOnly datetimeFormat As String = "yyMMddHHmmss"

    ''' <summary>
    ''' Dummy function for creating a basic history query
    ''' </summary>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <param name="data"></param>
    ''' <param name="confirmation"></param>
    ''' <returns></returns>
    Public Shared Function History(ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal data As String, ByVal confirmation As String)
        'TODO: If the responce value is truncated we need to continue reading the output stream, until it is no longer truncated (Output will only be truncated at a readout of some +450 days ..ish..).
        Return $"<history data=""{data}"" startdatetime=""{startDate.ToString(datetimeFormat)}"" enddatetime=""{endDate.ToString(datetimeFormat)}"" compression=""gzip"" confirmation=""{confirmation}"" addcolumns=""sync"" />"
    End Function
End Class
