﻿using System;
using System.Text;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string res = GetConsumption("user", "secret");
            Console.WriteLine(res);
        }

        static string GetConsumption(string username, string password)
        {
            var strBuilder = new StringBuilder();
            strBuilder.Append("<consumption type=\"electricity\" units=\"kWh\" confirmation=\"1234\" />\n");
            strBuilder.Append("<consumption type=\"water\" units=\"L\" confirmation=\"1235\" />\n");
            return Query(username, password, strBuilder.ToString());
        }

        static string Query(string username, string password, string command)
        {
            var strBuilder = new StringBuilder();
            strBuilder.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            strBuilder.Append($"<booking username=\"{username}\" password=\"{password}\">\n");
            strBuilder.Append("<house id=\"123456\">\n");
            strBuilder.Append(command);
            strBuilder.Append("</house>\n");
            strBuilder.Append("</booking>");

            return strBuilder.ToString();
        }



    }
}
